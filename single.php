<?php

/*
Theme Name: Wedding
*/
?><?php get_header(); ?>
	<div class="wrapper">
		<div class="content">
			<?php while(have_posts()) : the_post(); ?>
			<div class="left_content_2">
				<h2>Love Story</h2>
				<div class="featured_image">
					<?php the_post_thumbnail('medium');?>
				</div>
			</div>
			<div class="right_content_2">
				
				<?php the_title('<h1>','</h1>'); ?>
				
				<p><small>Posted by : <?php the_author(); ?> On: <?php the_date(); ?> At: <?php the_time(); ?></small></p>
				<?php the_content('<div>','</div>'); ?>
				
			<?php endwhile; ?>
			</div>
		</div><!--content ends-->

		<?php get_footer(); ?>