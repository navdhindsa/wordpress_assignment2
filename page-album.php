<?php
/*
Theme Name: Wedding
*/
?><?php get_header(); ?>
		
	<div class="wrapper">
		
		<div class="content">
			<?php while(have_posts()) : the_post() ?>

				<?php the_title(); ?>
			<?php endwhile; ?>
		</div><!--content ends-->

		<?php get_footer(); ?>