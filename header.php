<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- embedding Google font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charmonman" rel="stylesheet">
    

	<title>Wedding</title>

	
	<link type="text/css" rel="stylesheet" href="<?=get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="header_top">
			<div class="logo"><span class="pink">Preet</span>Aman</div>
			<div class="tagline">Our Wedding Page</div>
		</div>
		<nav>
			<div class="nav_inner">
				<?php wp_nav_menu('nav'); ?>
			</div>
		</nav>
	</header>