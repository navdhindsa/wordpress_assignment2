<?php

/*
Theme Name: Wedding
*/
?><?php get_header(); ?>
		
	<div class="wrapper">
		<div class="slider">
			<div class="slider_inner"><img src="<?=get_template_directory_uri();?>/1.jpg" alt="Slider Image" /></div><!--slider_inner ends -->
		</div><!--slider ends -->
		<div class="content">
			<div class="left_content"><h2>Love Story</h2>
				<?php get_sidebar(); ?>
			</div>
			<div class="right_content">
				<?php while(have_posts()) : the_post(); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title('<h1>','</h1>'); ?></a>
				
				<p><small>Posted by : <?php the_author(); ?> On: <?php the_date(); ?> At: <?php the_time(); ?></small></p>
				<?php the_excerpt('<div>','</div>'); ?>
				<a href="<?php the_permalink(); ?>">Read More</a>

				
			<?php endwhile; ?>
			</div>
		</div><!--content ends-->

		<?php get_footer(); ?>